Source: glare
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Ivan Udovichenko <iudovichenko@mirantis.com>,
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools (>= 74~),
 python3-all,
 python3-pbr (>= 2.0.0),
 python3-setuptools,
 python3-sphinx (>= 1.6.2),
Build-Depends-Indep:
 python3-alembic (>= 0.8.10),
 python3-babel,
 python3-bandit,
 python3-coverage,
 python3-cryptography (>= 1.9),
 python3-eventlet,
 python3-fixtures,
 python3-futurist (>= 1.2.0),
 python3-glance-store (>= 0.22.0),
 python3-hacking,
 python3-httplib2,
 python3-iso8601,
 python3-jsonpatch (>= 1.19+really1.16),
 python3-jsonschema (>= 2.6.0),
 python3-jwt,
 python3-keystoneauth1 (>= 3.2.0),
 python3-keystonemiddleware (>= 4.17.0),
 python3-memcache,
 python3-microversion-parse,
 python3-mock,
 python3-monotonic,
 python3-mox3 (>= 0.20.0),
 python3-mysqldb,
 python3-openssl,
 python3-os-api-ref (>= 1.4.0),
 python3-os-testr (>= 1.0.0),
 python3-oslo.concurrency (>= 3.20.0),
 python3-oslo.config (>= 1:4.6.0),
 python3-oslo.context (>= 2.14.0),
 python3-oslo.db (>= 4.27.0),
 python3-oslo.i18n (>= 3.15.3),
 python3-oslo.log (>= 3.30.0),
 python3-oslo.messaging (>= 5.29.0),
 python3-oslo.middleware (>= 3.31.0),
 python3-oslo.policy (>= 1.23.0),
 python3-oslo.serialization (>= 2.18.0),
 python3-oslo.service (>= 1.24.0),
 python3-oslo.utils (>= 3.28.0),
 python3-oslo.versionedobjects (>= 1.28.0),
 python3-oslosphinx,
 python3-oslotest (>= 1:1.10.0),
 python3-osprofiler,
 python3-paste,
 python3-pastedeploy,
 python3-psutil,
 python3-psycopg2 (>= 2.6.2),
 python3-reno,
 python3-requests (>= 2.14.2),
 python3-retrying,
 python3-routes,
 python3-semantic-version,
 python3-sendfile (>= 2.0.0),
 python3-six,
 python3-sqlalchemy,
 python3-swiftclient (>= 1:3.2.0),
 python3-testresources (>= 2.0.0),
 python3-testscenarios,
 python3-testtools,
 python3-webob (>= 1.7.1),
 python3-wsme,
 python3-xattr (>= 0.9.1),
 subunit,
 testrepository,
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/openstack-team/services/glare
Vcs-Git: https://salsa.debian.org/openstack-team/services/glare.git
Homepage: https://github.com/openstack/glare

Package: glare-api
Architecture: all
Depends:
 adduser,
 glare-common (= ${source:Version}),
 python3-openstackclient,
 q-text-as-data,
 ${misc:Depends},
 ${ostack-lsb-base},
 ${python3:Depends},
Description: OpenStack Artifact Repository - API server
 The Glance project provides services for discovering, registering, and
 retrieving virtual machine images over the cloud. They may be stand-alone
 services, or may be used to deliver images from object stores, such as
 OpenStack's Swift service, to Nova's compute nodes.
 .
 This package contains the Glare API server.

Package: glare-common
Architecture: all
Depends:
 adduser,
 dbconfig-common,
 python3-glare (= ${source:Version}),
 q-text-as-data,
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack Artifact Repository - common files
 Glare (from GLare Artifact REpository) is a service that provides access
 to a unified catalog of structured meta-information as well as related
 binary data (these structures are also called 'artifacts').
 .
 This package contains common files for Glare.

Package: python3-glare
Architecture: all
Section: python
Depends:
 python3-alembic (>= 0.8.10),
 python3-cryptography (>= 1.9),
 python3-eventlet,
 python3-futurist (>= 1.2.0),
 python3-glance-store (>= 0.22.0),
 python3-httplib2,
 python3-iso8601,
 python3-jsonpatch (>= 1.19+really1.16),
 python3-jsonschema (>= 2.6.0),
 python3-jwt,
 python3-keystoneauth1 (>= 3.2.0),
 python3-keystonemiddleware (>= 4.17.0),
 python3-memcache,
 python3-microversion-parse,
 python3-monotonic,
 python3-openssl,
 python3-oslo.concurrency (>= 3.20.0),
 python3-oslo.config (>= 1:4.6.0),
 python3-oslo.context (>= 2.14.0),
 python3-oslo.db (>= 4.27.0),
 python3-oslo.i18n (>= 3.15.3),
 python3-oslo.log (>= 3.30.0),
 python3-oslo.messaging (>= 5.29.0),
 python3-oslo.middleware (>= 3.31.0),
 python3-oslo.policy (>= 1.23.0),
 python3-oslo.serialization (>= 2.18.0),
 python3-oslo.service (>= 1.24.0),
 python3-oslo.utils (>= 3.28.0),
 python3-oslo.versionedobjects (>= 1.28.0),
 python3-osprofiler,
 python3-paste,
 python3-pastedeploy,
 python3-pbr (>= 2.0.0),
 python3-retrying,
 python3-routes,
 python3-semantic-version,
 python3-six,
 python3-sqlalchemy,
 python3-webob (>= 1.7.1),
 python3-wsme,
 ${misc:Depends},
 ${python3:Depends},
Conflicts:
 python-glare,
Description: OpenStack Artifact Repository - Python 3.x library
 The Glance project provides services for discovering, registering, and
 retrieving virtual machine images over the cloud. They may be stand-alone
 services, or may be used to deliver images from object stores, such as
 OpenStack's Swift service, to Nova's compute nodes.
 .
 This package contains the Python libraries.
